# Emoticons #

* Authors: Chris Leo, Noelia Ruiz Martínez, Mesar Hameed
* Download [stable version][1]
* Download [development version][2]

Using this add-on, spoken text containing emoticon characters will be replaced by its more human friendly description.
86 emoticons are defined.

For example: ":)" will be spoken as "smiling smiley", or ":D" will be spoken as "laughing smiley"

You can take advantage of the following features:

## Insert smiley ##

When you are unsure of the characters for a particular smiley, this addon enables you to select and insert it into your text such as in a chat.
Press NVDA+I, or from menu Preferences -> Manage emoticons -> Insert smiley, to open a dialog 
with the provided smilies.
When you press OK, the characters for the chosen smiley will be copied to your clipboard, ready for pasting.


## Customize emoticons ##

From NVDA MENU, Preferences -> Manage emoticons -> Customize emoticons, you can open a dialog setting to add or to edit available emoticons.
This dialog allows you to save an emoticons speech  dictionary  with your customizations.

Pressing "Save and export dictionary" button, a file dictionary named emoticons.dic will be saved in your user config folder, speechDicts subfolder.


## Activation settings ##

You can choose whether to Activate speaking of emoticons when starting NVDA. By default it is disabled.
It is also possible to save your choice for this setting.

## Key Commands: ##

*	NVDA+E: toggles between speaking text as it is written, or with the emoticons replaced by the human description.
*	NVDA+I: show a dialog to select a smiley you want to paste.


## Changes for 3.0 ##

* In the Customize emoticons dialog, it is now possible to specify that a pattern should only match if it is a whole word, according to speech dictionaries of NVDA 2014.4.


## Changes for 2.0 ##

* Add-on help is available from the Add-ons Manager.


## Changes for 1.1 ##

* Removed duplicated emoticon.
* Added some smileys.

## Changes for 1.0 ##

* Initial version.

[1]: http://addons.nvda-project.org/files/get.php?file=emo

[2]: http://addons.nvda-project.org/files/get.php?file=emo-dev
