# Emotikony #

* Autori: Chris Leo, Noelia Ruiz Martínez, Mesar Hameed
* stiahnuť [stabilná verzia][1]
* Stiahnuť [Vývojová verzia][2]

Using this add-on, spoken text containing emoticon characters will be
replaced by its more human friendly description.  86 emoticons are defined.

napríklad: ":)" bude prečítané ako "úsmev", alebo ":D" bude prečítané ako
"smiech"

Ďalšie vlastnosti:

## Vloženie emotikonu ##

Ak neviete, ako sa správne píše nejaký emotikon, môžete ho vybrať v zozname a priamo vložiť napríklad do chatu.
Stlačte nvda+i, alebo v Menu NVDA > Možnosti > emotikony vyberte položku vložiť.
Zobrazí sa dialóg s dostupnými tvárami.
Po stlačení OK sa znaky skopírujú do schránky.


## vlastné emotikony ##

V menu NVDA >možnosti >emotikony >pridať /upraviť môžete vytvoriť nové, alebo upraviť existujúce emotikony.
V tomto dialógu môžete uložiť vlastný používateľský slovník s emotikonmi.

Pressing "Save and export dictionary" button, a file dictionary named
emoticons.dic will be saved in your user config folder, speechDicts
subfolder.


## Aktivácia ##

môžete určiť, či má alebo nemá byť doplnok aktívny po štarte NVDA. Tiež
môžete určiť predvolené nastavenie do budúcnosti.

## Klávesové skratky: ##

*	NVDA+E: prepína medzi vyslovovaním znakov a popisov emotikonov.
*	NVDA+I: zobrazí dialóg na vloženie požadovaného úsmevu.


## Changes for 2.0 ##

* Add-on help is available from the Add-ons Manager.

## Changes for 1.1 ##

* Removed duplicated emoticon.
* Added some smileys.

## Zmeny vo verzii 1.0 ##

* prvé vydanie.

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=emo

[2]: http://addons.nvda-project.org/files/get.php?file=emo-dev
