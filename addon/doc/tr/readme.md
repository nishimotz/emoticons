# İfadeler #

* Yazarlar: Chris Leo, Noelia Ruiz Martínez, Mesar HameedYazarlar: Chris,
  Noelia Ruiz Martínez, Mesar Hameed
* İndir [kararlı sürüm][1]
* İndir [geliştirme sürümü][2]

Using this add-on, spoken text containing emoticon characters will be
replaced by its more human friendly description.  86 emoticons are defined.

For example: ":)" will be spoken as "smiling smiley", or ":D" will be spoken
as "laughing smiley"

Aşağıdaki özelliklerden yararlanabilirsiniz:

## İfade ekle ##

Belirli bir ifadenin hangi karakterlerden oluştuğundan emin değilseniz, Bu eklenti sohbet ettiğiniz ekranlar gibi metinler için surat seçmenizi ve eklemenizi sağlar.
NVDA + I tuşlarına basın ya da tercihler menüsünden ifadeleri yönet altından ifade ekle seçeneğiyle
mevcut ifadelerin listesinin bulunduğu iletişim kutusunu açın.
Tamam düğmesine bastığınızda, seçilen ifadeyle ilgili karakterler panoya kopyalanmış olacak.


## İfadeleri özelleştir ##

NVDA menüsü altından, Tercihler -> ifadeleri yönet -> ifadeleri özelleştir seçeneğiyle, kullanılabilir ifadeleri eklemek veya düzenlemek için bir iletişim kutusu açabilirsiniz.
Bu iletişim kutusu özelleştirmelerinizle birlikte bir ifade konuşma sözlüğü kaydetmenize olanak verir.

Pressing "Save and export dictionary" button, a file dictionary named
emoticons.dic will be saved in your user config folder, speechDicts
subfolder.


## Etkinleştirme ayarları ##

NVDA başlayınca ifade eklentisinin etkin olup olmayacağını
belirleyebilirsiniz. Varsayılan olarak devre dışıdır.  Bu ayar için
seçiminizi kalıcı olarak kaydetmek mümkündür.

## Tuş Komutları: ##

*	NVDA+E: ifadenin yazıldığı şekilde mi yoksa tanımlandığı haliyle mi
  seslendirileceği ile ilgili ayarı değiştirir.
*	NVDA + I: yapıştırmak istediğiniz bir ifade seçmek için bir iletişim
  kutusu açar.


## Changes for 2.0 ##

* Add-on help is available from the Add-ons Manager.

## Changes for 1.1 ##

* Removed duplicated emoticon.
* Added some smileys.

## 1.0 için değişiklikler ##

* İlk sürüm.

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=emo

[2]: http://addons.nvda-project.org/files/get.php?file=emo-dev
