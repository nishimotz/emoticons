# Emoticons #

* Auteurs: Chris Leo, Noelia Ruiz Martínez, Mesar Hameed
* Download [stabiele versie][1]
* Download [ontwikkelversie][2]

Using this add-on, spoken text containing emoticon characters will be
replaced by its more human friendly description.  86 emoticons are defined.

Bijvoorbeeld: ":)" zal uitgesproken worden als "smiling smiley", of ":D" zal
uitgesproken worden als "laughing smiley"

U kunt gebruik maken van de volgende functies:

## Smiley invoegen ##

Als u niet zeker weet met welke karakters u een bepaalde smiley maakt, kunt u in deze addon smileys selecteren en invoegen in uw tekst, bijvoorbeeld tijdens het chatten.
Press NVDA+I, or from menu Preferences -> Manage emoticons -> Insert smiley, to open a dialog 
with the provided smilies.
When you press OK, the characters for the chosen smiley will be copied to your clipboard, ready for pasting.


## Emoticons aanpassen ##

In het NVDA menu, Opties -> Emoticons beheren -> Emoticons personaliseren, kunt u beschikbare emoticons toevoegen of bewerken.
Dit dialoogvenster laat u toe een emoticons uitspraakwoordenboek op te slaan met uw aanpassingen.

Pressing "Save and export dictionary" button, a file dictionary named
emoticons.dic will be saved in your user config folder, speechDicts
subfolder.


## Actievatie-instellingen ##

U kunt kiezen of u het uitspreken van emoticons wilt inschakelen bij het
starten van NVDA. Deze optie is standaard uitgeschakeld. Het is ook mogelijk
om uw keuze op te slaan voor deze instelling.

## Toetscommando's: ##

*	NVDA+E: Schakelt tussen het uitspreken van tekst zoals deze geschreven is
  en het vervangen van emoticonkarakters door omschrijvingen.
*	NVDA+I: geeft een dialoogvenster weer waarin u een smiley kunt selecteren
  die u vervolgens kunt plakken.


## Changes for 2.0 ##

* Add-on help is available from the Add-ons Manager.

## Changes for 1.1 ##

* Removed duplicated emoticon.
* Added some smileys.

## Veranderingen voor 1.0 ##

* Eerste versie.

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=emo

[2]: http://addons.nvda-project.org/files/get.php?file=emo-dev
