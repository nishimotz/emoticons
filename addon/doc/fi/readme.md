# Hymiöt #

* Tekijät: Chris Leo, Noelia Ruiz Martínez, Mesar Hameed
* Lataa [vakaa versio][1]
* Lataa [kehitysversio][2]

Using this add-on, spoken text containing emoticon characters will be
replaced by its more human friendly description.  86 emoticons are defined.

Esimerkki: ":)" luetaan "hymyilee" tai ":D" luetaan "nauraa"

Voit hyödyntää seuraavia ominaisuuksia:

## Lisää hymiö ##

Kun olet epävarma jonkin määrätyn hymiön merkeistä, tämän lisäosan avulla voit valita ja lisätä ne tekstiisi, kuten esim chattiin.
Paina NVDA+I tai valitse NVDA-valikosta Asetukset -> Hymiöiden hallinta -> Lisää hymiö avataksesi valintaikkunan, 
jossa käytettävissä olevat hymiöt näytetään.
Kun painat OK, valitsemasi hymiön merkit kopioidaan leikepöydälle, josta voit liittää ne haluamaasi paikkaan.


## Mukauta hymiöitä ##

Voit avata valintaikkunan uusien hymiöiden lisäämiseksi tai olemassa olevien muokkaamiseksi valitsemalla NVDA-valikosta Asetukset -> Hymiöiden hallinta -> Mukauta hymiöitä.
Tekemäsi muokkaukset tallennetaan hymiöiden puhesanastoon.

Pressing "Save and export dictionary" button, a file dictionary named
emoticons.dic will be saved in your user config folder, speechDicts
subfolder.


## Käyttöönoton asetukset ##

Voit valita, otetaanko hymiöiden puhuminen käyttöön NVDA:ta
käynnistettäessä. Tämä ei ole oletusarvoisesti käytössä.  Valitsemasi asetus
on myös mahdollista tallentaa.

## Näppäinkomennot: ##

*	NVDA+E: ottaa käyttöön tekstin lukemisen sellaisena kuin se on kirjoitettu
  tai hymiöiden korvaamisen niiden kuvauksilla.
*	NVDA+I: näyttää valintaikkunan, josta voit valita tekstiin liitettävän
  hymiön.


## Changes for 2.0 ##

* Add-on help is available from the Add-ons Manager.

## Changes for 1.1 ##

* Removed duplicated emoticon.
* Added some smileys.

## Muutokset versiossa 1.0 ##

* Ensimmäinen versio.

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=emo

[2]: http://addons.nvda-project.org/files/get.php?file=emo-dev
