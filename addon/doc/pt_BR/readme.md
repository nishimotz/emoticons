# Emoticons #

* Autores: Chris Leo, Noelia Ruiz Martínez, Mesar Hameed
* Baixe a [versão estável][1]
* Baixe a [versão de desenvolvimento][2]

Using this add-on, spoken text containing emoticon characters will be
replaced by its more human friendly description.  86 emoticons are defined.

Por exemplo: ":)" será falado como "cara de sorriso", ou ":D" will be spoken
as "cara de riso"

Pode tirar proveito dos seguintes recursos:

## Inserir cara ##

Quando você está em dúvida sobre quais são os caracteres para uma cara em particular, este complemento o abilita a selecioná-los e inseri-los num texto, por exemplo um bate-papo.
Pressione NVDA+I, ou no menu Preferências -> Gerir emoticons -> Inserir cara, para abrir um diálogo 
com as caras disponíveis.
Quando pressionar OK, os caracteres da cara escolhida serão copiados para a área de transferência, prontos para colagem.


## Personalizar emoticons ##

No menu do NVDA, em Preferências -> Gerir emoticons -> personalizar emoticons, pode abrir um diálogo de opções para adicionar ou editar os emoticons disponíveis.
Esse diálogo lhe possibilita salvar um dicionário de fala de emoticons com as suas personalizações.

Pressing "Save and export dictionary" button, a file dictionary named
emoticons.dic will be saved in your user config folder, speechDicts
subfolder.


## Opções de ativação ##

Você pode escolher se quer ativar o anúncio de emoticons quando o NVDA
inicia. Por padrão, está desabilitado.  Também é possível salvar sua escolha
para esta opção.

## Teclas de comando: ##

*	NVDA+E: alterna entre falar o texto como está escrito ou com os emoticons
  substituídos pelas descrições amigáveis.
*	NVDA+I: mostra um diálogo para selecionar uma cara que queira colar.


## Changes for 2.0 ##

* Add-on help is available from the Add-ons Manager.

## Changes for 1.1 ##

* Removed duplicated emoticon.
* Added some smileys.

## Mudanças na 1.0 ##

* Versão inicial.

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=emo

[2]: http://addons.nvda-project.org/files/get.php?file=emo-dev
