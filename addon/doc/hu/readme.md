# Hangulatjelek #

* Készítők: Chris Leo, Noelia Ruiz Martínez, Mesar Hameed
* Letöltés [Stabil verzió][1]
* Letöltés [Fejlesztői verzió][2]

Using this add-on, spoken text containing emoticon characters will be
replaced by its more human friendly description.  86 emoticons are defined.

Például: ":)" helyett a "Mosolygó arc" kerül kimondásra, a ":D" helyett
pedig "Nevető arc".

A következő lehetőségek állnak rendelkezésére:

## Hangulatjel beszúrása ##

Ha nem biztos egy hangulatjel karakterkódjában, a kkiegészítő segít annak beszúrásában.
Nyomja meg az NVDA+i billentyűparancsot, vagy aktiválja az NVDA menü ->beállítások->Hangulatjelek kezelése->Hangulatjel beszúrása menüpontot.
A megnyíló párbeszédablakban kiválaszthatja a beszúrandó hangulatjelet, ami a vágólapra kerül az "Igen" megnyomásakor.


## Hangulatjelek testreszabása ##

Az NVDA menü ->beállítások->Hangulatjelek kezelése ->hangulatjelek testreszabása menüpontban hozzáadhat, vagy szerkeszthet egy már meglévő hangulatjelet.
Lehetőség van egy kivételszótárba elmenteni a testreszabott hangulatjel listát.

Pressing "Save and export dictionary" button, a file dictionary named
emoticons.dic will be saved in your user config folder, speechDicts
subfolder.


## Aktiválási beállítások ##

Kiválaszthatja a hangulatjelek alapértelmezett bemondását az NVDA
indításakor, ez alapból ki van kapcsolva. Lehetősége van a beállítás
elmentésére.

## Billentyű parancsok: ##

*	NVDA+E: Be és kikapcsolható a hangulatjel szöveges bemondása.
*	NVDA+I: Megjeleníti a hangulatjel beszúrására szolgáló párbeszédablakot.


## Changes for 2.0 ##

* Add-on help is available from the Add-ons Manager.

## Changes for 1.1 ##

* Removed duplicated emoticon.
* Added some smileys.

## Az 1.0 változásai ##

* Első verzió

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=emo

[2]: http://addons.nvda-project.org/files/get.php?file=emo-dev
