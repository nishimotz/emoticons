# Emotikony / Emoticons #

* Autorzy: Chris Leo, Noelia Ruiz Martínez, Mesar Hameed
* Pobierz [wersja stabilna][1]
* Pobierz [wersja rozwojowa][2]

Using this add-on, spoken text containing emoticon characters will be
replaced by its more human friendly description.  86 emoticons are defined.

Dla przykładu: ":)" zostanie odczytane jako "Buzia uśmiech", albo ":D"
zostanie przeczytane jako "Buzia śmiech"

Możesz wykorzystać następujące funkcje:

## Wstaw uśmieszek ##

Jeśli nie jesteś pewien z jakich znaków składa się konkretny uśmieszek, ten dodatek pozwala wybrać go i wstawić do twojego tekstu, np. na czacie.
Aby otworzyć okno dialogowe z dostępnymi emotikonami,
 naciśnij NVDA+I, lub wybierz z menu Ustawienia-> Zarządzaj emotikonami -> Wstaw emotikon.
Po naciśnięciu OK, znaki tworzące wybrany emotikon zostaną skopiowane do schowka, gotowe do wklejenia.


## Dostosuj emotikony ##

Z menu NVDA, możesz wybrać Ustawienia-> Zarządzaj emotikonami-> Dostosuj emotikony, aby otworzyć okno ustawień, umożliwiające dodawanie lub edycję dostępnych emotikon.
To okno umożliwia zapisanie słownika wymowy emotikon zawierającego twoje dostosowania.

Pressing "Save and export dictionary" button, a file dictionary named
emoticons.dic will be saved in your user config folder, speechDicts
subfolder.


## Ustawienia aktywacji ##

Możesz ustawić aktywowanie wypowiadania emotikon po starcie NVDA. Domyślnie
jest wyłączone.  Możliwe jest również zapisanie twojego wyboru tego
ustawienia.

## Skróty klawiszowe: ##

*	NVDA+E: przełącza między odczytem tekstu tak jak jest napisany, albo z
  emotikonami zastąpionymi przez opisy.
*	NVDA+I: wyświetla okno dialogowe wyboru uśmieszku do wklejenia.


## Changes for 2.0 ##

* Add-on help is available from the Add-ons Manager.

## Changes for 1.1 ##

* Removed duplicated emoticon.
* Added some smileys.

## Zmiany dla wersji 1.0 ##

* Pierwsza wersja.

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=emo

[2]: http://addons.nvda-project.org/files/get.php?file=emo-dev
