# Emoticons #

* Authoren: Chris Leo, Noelia Ruiz Martínez, Mesar Hameed
* [stabile Version herunterladen][1]
* [Testversion herunterladen][2]

Using this add-on, spoken text containing emoticon characters will be
replaced by its more human friendly description.  86 emoticons are defined.

So wird beispielsweise: ":)" als "Lächelndes Smiley", oder ":D" als
"lachendes Smiley" gesprochen.

You can take advantage of the following features:

## Smiley einfügen ##

When you are unsure of the characters for a particular smiley, this addon enables you to select and insert it into your text such as in a chat.
Press NVDA+I, or from menu Preferences -> Manage emoticons -> Insert smiley, to open a dialog 
with the provided smilies.
When you press OK, the characters for the chosen smiley will be copied to your clipboard, ready for pasting.


## Customize emoticons ##

From NVDA MENU, Preferences -> Manage emoticons -> Customize emoticons, you can open a dialog setting to add or to edit available emoticons.
This dialog allows you to save an emoticons speech  dictionary  with your customizations.

Pressing "Save and export dictionary" button, a file dictionary named
emoticons.dic will be saved in your user config folder, speechDicts
subfolder.


## Aktivierungseinstellungen ##

You can choose whether to Activate speaking of emoticons when starting
NVDA. By default it is disabled.  It is also possible to save your choice
for this setting.

## Tastenkombinationen: ##

*	nvda+e: legt fest, ob Text so gelesen wird, wie er geschrieben wurde oder
  ob Emoticons durch die menschliche Beschreibung ersetzt werden sollen.
*	NVDA+I: show a dialog to select a smiley you want to paste.


## Changes for 2.0 ##

* Add-on help is available from the Add-ons Manager.

## Changes for 1.1 ##

* Removed duplicated emoticon.
* Added some smileys.

## Änderungen in 1.0 ##

* Ehrstveröffentlichung

[[!tag dev stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=emo

[2]: http://addons.nvda-project.org/files/get.php?file=emo-dev
